/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.og.ws.dto;

/**
 *
 * @author tdim
 */
public class ResponseDTO {
    private String taskId;

    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
    
}
