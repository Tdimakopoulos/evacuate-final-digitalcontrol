/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evacuate.digitalcontrol;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.DigitalSignDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.evacuate.sensors.SensorsBean;
import org.evacuate.sensors.sensordata;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class EvacuateDigitalControl {

    public boolean FileExists(String filePathString) {
        File f = new File("c:\\evacctrl\\" + filePathString);
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public void writeFile2(String file, String line) throws IOException {
        FileWriter fw = new FileWriter("c:\\evacctrl\\" + file);

        fw.write(line);

        fw.close();
    }

    public void DeleteFile(String filepath) {
        try {

            File file = new File("c:\\evacctrl\\" + filepath);

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
//                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

//            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
//        playMedia(new Long(1621054),new Long(1637633));
//        Thread.sleep(20000);
//        stopMedia(new Long(1621054));
        System.out.println("Starting Digital Control (Version : Final)");
        SensorsBean psb = new SensorsBean();
        EvacuateDigitalControl pp = new EvacuateDigitalControl();
        
        while (true) {
            psb.ReLoad();
            List<sensordata> SensorData = psb.getPdata();
            for (int i = 0; i < SensorData.size(); i++) {
                String devid = SensorData.get(i).getDeviceid();
                String devcom = SensorData.get(i).getCommand();
                String devpcom = SensorData.get(i).getPreviouscommand();
                String devtype = SensorData.get(i).getDevicetype();

                if (pp.FileExists("vv.vv")) {
                    
                        pp.DeleteFile("done");
                        
                        if (devid.equalsIgnoreCase("tek-aes-15")) {
                            System.out.println("Device Found Command : " + devcom + " - Previous Command : " + devpcom);

                            if (devcom.equalsIgnoreCase("UP")) {
                                playMedia(new Long(1621053), new Long(1637633));
                                System.out.println("UP");
                            }

                            if (devcom.equalsIgnoreCase("DOWN")) {
                                playMedia(new Long(1621053), new Long(1637633));
                                System.out.println("DOWN");
                            }

                            if (devcom.equalsIgnoreCase("LEFT")) {
                                playMedia(new Long(1621053), new Long(1637633));
//set left
                                System.out.println("LEFT");
                            }
                            if (devcom.equalsIgnoreCase("RIGHT")) {
                                playMedia(new Long(1621053), new Long(1637635));
                                //set left
                                System.out.println("RIGHT");
                            }
                            if (devcom.equalsIgnoreCase("OFF")) {
                                //set left
                                stopMedia(new Long(1621053));
                                System.out.println("OFF");
                            }
                            if (devcom.equalsIgnoreCase("CLOSED")) {
                                playMedia(new Long(1621053), new Long(1637615));
                                //set left
                                System.out.println("CLOSED");
                            }
                        }

                        if (devid.equalsIgnoreCase("tek-aes-16")) {
                            System.out.println("Device Found Command : " + devcom + " - Previous Command : " + devpcom);

                            if (devcom.equalsIgnoreCase("UP")) {
                                playMedia(new Long(1637638), new Long(1637633));
                                System.out.println("UP");
                            }

                            if (devcom.equalsIgnoreCase("DOWN")) {
                                playMedia(new Long(1637638), new Long(1637633));
                                System.out.println("DOWN");
                            }

                            if (devcom.equalsIgnoreCase("LEFT")) {
                                playMedia(new Long(1637638), new Long(1637633));
//set left
                                System.out.println("LEFT");
                            }
                            if (devcom.equalsIgnoreCase("RIGHT")) {
                                playMedia(new Long(1637638), new Long(1637635));
                                //set left
                                System.out.println("RIGHT");
                            }
                            if (devcom.equalsIgnoreCase("OFF")) {
                                //set left
                                stopMedia(new Long(1637638));
                                System.out.println("OFF");
                            }
                            if (devcom.equalsIgnoreCase("CLOSED")) {
                                playMedia(new Long(1637638), new Long(1637615));
                                //set left
                                System.out.println("CLOSED");
                            }
                        }

                    
                } else {
                    if (pp.FileExists("done")) {
                    } else {
                        
                        stopMedia(new Long(1621053));
                        stopMedia(new Long(1637638));
                        System.err.println("OFF");
                        try {
                            pp.writeFile2("done", "1");
                        } catch (IOException ex) {
                            Logger.getLogger(EvacuateDigitalControl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                }
            }
            Thread.sleep(20000);
        }
    }

    private static String playMedia(java.lang.Long signID, java.lang.Long fileID) {
        eu.exus.digitalsign.ws.DigitalSignMediaManager_Service service = new eu.exus.digitalsign.ws.DigitalSignMediaManager_Service();
        eu.exus.digitalsign.ws.DigitalSignMediaManager port = service.getDigitalSignMediaManagerPort();
        return port.playMedia(signID, fileID);
    }

    private static String stopMedia(java.lang.Long signID) {
        eu.exus.digitalsign.ws.DigitalSignMediaManager_Service service = new eu.exus.digitalsign.ws.DigitalSignMediaManager_Service();
        eu.exus.digitalsign.ws.DigitalSignMediaManager port = service.getDigitalSignMediaManagerPort();
        return port.stopMedia(signID);
    }

}
